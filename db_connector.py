import mysql.connector
from mysql.connector.abstracts import MySQLConnectionAbstract
from mysql.connector.pooling import PooledMySQLConnection

def connect_to_database() -> PooledMySQLConnection | MySQLConnectionAbstract:
    return mysql.connector.connect(
        host="localhost",
        port="43306",
        user="root",
        password="mysql",
        database="yelp_db"
    )
