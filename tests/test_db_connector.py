import pytest
from db_connector import connect_to_database

@pytest.fixture
def db_connection():
    connection = connect_to_database()
    yield connection
    connection.close()

def test_db_connection(db_connection):
    assert db_connection.is_connected()

