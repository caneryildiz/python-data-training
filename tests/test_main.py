import pandas as pd
import pytest
import csv
from main import (
    main,
    process_data_pure_python,
    process_data_pandas,
    count_data,
    combine_data,
    write_to_csv
)


def test_count_data():
    # Test case 1: Empty data
    result = count_data([])
    assert result == {}

    # Test case 2: Normal case with multiple tuples
    data = [(1, 'A'), (2, 'B'), (1, 'A'), (3, 'A'), (2, 'B')]
    result = count_data(data)
    expected_result = {'A': 3, 'B': 2}
    assert result == expected_result

    # Test case 3: Single tuple
    data = [(1, 'A')]
    result = count_data(data)
    expected_result = {'A': 1}
    assert result == expected_result


def test_combine_data():
    business_results = [(1,), (2,), (3,)]
    tip_results = {'1': 5, '2': 2, '3': 0}
    review_results = {'1': 3, '2': 1, '3': 4}
    result = combine_data(business_results, tip_results, review_results)
    expected_result = [[1, 5, 3], [2, 2, 1], [3, 0, 4]]
    assert result == expected_result


def test_write_to_csv(tmp_path):
    filename = tmp_path / "test_output.csv"
    header = ['business_id', 'tips_count', 'reviews_count']
    data = [[1, 5, 3], [2, 2, 1], [3, 0, 4]]
    write_to_csv(str(filename), header, data)

    with open(filename, 'r') as file:
        csv_content = file.read()

    expected_content = "business_id,tips_count,reviews_count\n1,5,3\n2,2,1\n3,0,4\n"
    assert csv_content == expected_content

def main_setup(mocker, choice: str):
    mock_db_connection = mocker.MagicMock()
    mocker.patch('builtins.input', return_value=choice)

    mocker.patch('db_connector.connect_to_database', return_value=mock_db_connection)
    mocker.patch.object(mock_db_connection, 'cursor', return_value=mocker.MagicMock())

    mock_get_business_data = mocker.patch('main.get_business_data', return_value="mock_business_data")
    mock_get_tip_data = mocker.patch('main.get_tip_data', return_value="mock_tip_data")
    mock_get_review_data = mocker.patch('main.get_review_data', return_value="mock_review_data")
    return mock_get_business_data, mock_get_tip_data, mock_get_review_data


def test_main_with_choice_1(mocker):
    mock_get_business_data, mock_get_tip_data, mock_get_review_data = main_setup(mocker, '1')
    mock_process_data_pure_python = mocker.patch('main.process_data_pure_python')
    mocker.patch('main.process_data_pandas')  # Mocking process_data_pandas , so it doesn't get called

    main()

    mock_get_business_data.assert_called_once()
    mock_get_tip_data.assert_called_once_with(mocker.ANY, 2012)
    mock_get_review_data.assert_called_once_with(mocker.ANY, 2005)
    mock_process_data_pure_python.assert_called_once_with("mock_business_data", "mock_tip_data", "mock_review_data")


def test_main_with_choice_2(mocker):
    mock_get_business_data, mock_get_tip_data, mock_get_review_data = main_setup(mocker, '2')
    mock_process_data_pandas = mocker.patch('main.process_data_pandas')
    mocker.patch('main.process_data_pure_python')  # Mocking process_data_pandas , so it doesn't get called

    main()

    mock_get_business_data.assert_called_once()
    mock_get_tip_data.assert_called_once_with(mocker.ANY, 2012)
    mock_get_review_data.assert_called_once_with(mocker.ANY, 2005)
    mock_process_data_pandas.assert_called_once_with("mock_business_data", "mock_tip_data", "mock_review_data")

def read_content_of_csv(file_path):
    with open(file_path, 'r', newline='', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile)
        content = [row for row in reader]
    return content


def test_process_data_pure_python():
    expected_data = [
        ["business_id", "tips_count", "reviews_count"],
        [1, 3, 3],
        [2, 1, 2]
    ]

    business_results = [(1,), (2,)]
    tip_results = [(1, '1'), (1, '1'), (4, '1'), (2, '2')]
    review_results = [(1, '1'), (2, '1'), (4, '1'), (2, '2'), (1, '2')]

    process_data_pure_python(business_results, tip_results, review_results)

    with open('output.csv', 'r') as csvfile:
        csv_reader = csv.reader(csvfile)
        # Skip the header row
        next(csv_reader)
        # Convert each element in each row to int and creates a list of ints.
        generated_data = [list(map(int, row)) for row in csv_reader]

    # [1:] To skip header row.
    assert generated_data == expected_data[1:]


def test_process_data_pandas(capsys):
    business_results = [{'business_id': '1'}, {'business_id': '2'}]
    tip_results = [{'business_id': '1'}, {'business_id': '1'}, {'business_id': '2'}]
    review_results = [{'business_id': '1'}, {'business_id': '2'}]

    process_data_pandas(business_results, tip_results, review_results)

    actual_df = pd.read_csv("output_pandas.csv")

    expected_df = pd.DataFrame([
        {'business_id': 1, 'tips_count': 2, 'reviews_count': 1},
        {'business_id': 2, 'tips_count': 1, 'reviews_count': 1}
    ])
    assert expected_df.equals(actual_df)
    captured = capsys.readouterr()
    assert "CSV file 'output_pandas.csv' saved successfully." in captured.out


if __name__ == "__main__":
    pytest.main(["-v", "test_main_script.py"])
