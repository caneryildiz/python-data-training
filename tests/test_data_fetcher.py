import pytest
from data_fetcher import (
    execute_query,
    get_business_data,
    get_tip_data,
    get_review_data
)
from mysql.connector import connect
from mysql.connector.errors import ProgrammingError

@pytest.fixture(scope="module")
def db_connection():
    connection = connect(
        host="localhost",
        port="43306",
        user="root",
        password="mysql",
        database="yelp_db"
    )
    yield connection
    connection.close()

@pytest.fixture(scope="module")
def cursor(db_connection):
    return db_connection.cursor(dictionary=True)

def test_execute_query(cursor):
    result = execute_query(cursor, "SELECT 1;")
    assert result == [{'1': 1}]

def test_get_business_data(cursor):
    result = get_business_data(cursor)
    assert isinstance(result, list)

def test_get_tip_data(cursor):
    year = 2012
    result = get_tip_data(cursor, year)
    assert isinstance(result, list)

def test_get_review_data(cursor):
    year = 2005
    result = get_review_data(cursor, year)
    assert isinstance(result, list)

def test_execute_query_invalid_query(cursor):
    with pytest.raises(ProgrammingError):
        execute_query(cursor, "INVALID SQL QUERY;")


if __name__ == "__main__":
    pytest.main(["-v", "test_data_fetcher.py"])
