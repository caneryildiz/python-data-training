import csv
from collections import Counter
from typing import List, Tuple, Dict

from data_fetcher import get_business_data, get_tip_data, get_review_data
from db_connector import connect_to_database

import pandas as pd

def count_data(data: List[Tuple[int, str]]) -> Dict[str, int]:
    return dict(Counter(business_id for _, business_id in data))

def combine_data(business_results: List[Tuple], tip_results: Dict[str, int], review_results: Dict[str, int]) -> List[List]:
    combined_data = []
    for (business_id,) in business_results:
        tips_count = tip_results.get(str(business_id), 0)
        reviews_count = review_results.get(str(business_id), 0)
        combined_data.append([business_id, tips_count, reviews_count])
    return combined_data

def write_to_csv(filename: str, header: List[str], data: List[List]) -> None:
    with open(filename, 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(header)
        csv_writer.writerows(data)
    print(f"CSV file '{filename}' saved successfully.")

def main() -> None:
    print("Choose the version:")
    print("1. Pure Python")
    print("2. Pandas")

    choice = input("Enter the number corresponding to your choice: ")

    db_connection = connect_to_database()
    cursor = db_connection.cursor(dictionary=False)

    business_results = get_business_data(cursor)
    tip_results = get_tip_data(cursor, 2012)
    review_results = get_review_data(cursor, 2005)

    if choice == "2":
        process_data_pandas(business_results, tip_results, review_results)
    else:
        process_data_pure_python(business_results, tip_results, review_results)
    cursor.close()


def process_data_pure_python(business_results, tip_results, review_results):
    tips_count_dict = count_data(tip_results)
    reviews_count_dict = count_data(review_results)
    combined_data = combine_data(business_results, tips_count_dict, reviews_count_dict)
    write_to_csv('output.csv', ['business_id', 'tips_count', 'reviews_count'], combined_data)


def process_data_pandas(business_results: list, tip_results: list, review_results: list):
    business_df = pd.DataFrame(business_results, columns=['business_id'])

    tip_df = pd.DataFrame(tip_results, columns=['id', 'business_id'])
    tips_count_df = tip_df.groupby('business_id').size().reset_index(name='tips_count')

    review_df = pd.DataFrame(review_results, columns=['id', 'business_id'])
    reviews_count_df = review_df.groupby('business_id').size().reset_index(name='reviews_count')

    combined_data_df = pd.merge(business_df, tips_count_df, how='left', on='business_id').fillna(0)
    combined_data_df["tips_count"] = combined_data_df["tips_count"].astype(int)

    combined_data_df = pd.merge(combined_data_df, reviews_count_df, how='left', on='business_id').fillna(0)
    combined_data_df["reviews_count"] = combined_data_df["reviews_count"].astype(int)

    combined_data_df.to_csv('output_pandas.csv', index=False)
    print("CSV file 'output_pandas.csv' saved successfully.")


if __name__ == "__main__":
    main()
