from typing import List, Tuple
from mysql.connector.abstracts import MySQLCursorAbstract
from sql_queries import TIP_QUERY, BUSINESS_QUERY, REVIEW_QUERY


def execute_query(cursor: MySQLCursorAbstract, query: str, *params) -> List[Tuple]:
    cursor.execute(query, params)
    return cursor.fetchall()

def get_business_data(cursor: MySQLCursorAbstract) -> List[Tuple]:
    return execute_query(cursor, BUSINESS_QUERY)

def get_tip_data(cursor: MySQLCursorAbstract, year: int) -> List[Tuple]:
    return execute_query(cursor, TIP_QUERY, year)

def get_review_data(cursor: MySQLCursorAbstract, year: int) -> List[Tuple]:
    return execute_query(cursor, REVIEW_QUERY, year)
